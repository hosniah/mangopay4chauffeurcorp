<?php
require_once '../MangoPaySDK/mangoPayApi.inc';

class ChauffeurCorp_BankAccount {

	private $UserId;
	private $OwnerName;
	private $OwnerAddress;
	private $DetailsIBAN;
	private $DetailsBIC;
	private $Tag;

	const TYPE = "IBAN";

	public function __construct ($UserId, $OwnerName, $OwnerAddress, $DetailsIBAN, $DetailsBIC, $Tag=NULL) {
		$this->UserId       = $UserId;
		$this->OwnerName    = $OwnerName ;
		$this->OwnerAddress = $OwnerAddress;
		$this->DetailsIBAN  = $DetailsIBAN;
		$this->DetailsBIC   = $DetailsBIC;
	}
	private function getMangoPayApi() {
		$mangoPayApi  = new MangoPay\MangoPayApi();
		$mangoPayApi->Config->ClientId        = 'frenchconnection2015';
		$mangoPayApi->Config->ClientPassword  = '5CsHKZGBDxv7btyoeMXCbLBKT59VqsJUMSedLTXhv1WveqNBOo';
		$mangoPayApi->Config->TemporaryFolder = __dir__;
		return $mangoPayApi;
	}

	public function createBankAccount() {
		//Create an instance of MangoPayApi SDK
		$mangoPayApi = $this->getMangoPayApi();

		//Build the parameters for the request
		$UserId               = $this->UserId;
		$BankAccount          = new MangoPay\BankAccount();
		$BankAccount->Type    = "IBAN";
		//$BankAccount->Details = new MangoPay\BankAccountDetailsOther();

		$BankAccount->Details       = new MangoPay\BankAccountDetailsIBAN();
		$BankAccount->Details->IBAN = $this->DetailsIBAN;
		$BankAccount->Details->BIC  = $this->DetailsBIC;
		$BankAccount->OwnerName     = $this->OwnerName;
		$BankAccount->OwnerAddress  = $this->OwnerAddress;
		 
		//Send the request
		$result = $mangoPayApi->Users->CreateBankAccount($UserId, $BankAccount);
		 
		//Analyse the request
		//var_dump($result);
		return $result;
	}

}

//Usage:
$bankAccountManager = new ChauffeurCorp_BankAccount( 5283321, "Brian Boitano", "1 rue des martyrs", "FR3020041010124530725S03383", "CRLYFRPP");
$tty = $bankAccountManager->createBankAccount();
var_dump($tty);
