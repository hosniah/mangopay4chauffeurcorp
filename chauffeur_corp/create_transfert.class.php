<?php
require_once '../MangoPaySDK/mangoPayApi.inc';

class ChauffeurCorp_Transfers {
	private $AuthorId;
	private $DebitedFunds_Currency;
	private $DebitedFunds_Amount;
	private $Fees_Currency;
	private $Fees_Amount;
	private $DebitedWalletId;
	private $CreditedWalletId;
	

	public function __construct ($AuthorId, $DebitedFunds_Currency, $DebitedFunds_Amount, $Fees_Currency, $Fees_Amount, $DebitedWalletId, $CreditedWalletId) {
		$this->AuthorId              = $AuthorId;
		$this->DebitedFunds_Currency = $DebitedFunds_Currency;
		$this->DebitedFunds_Amount   = $DebitedFunds_Amount;
		$this->Fees_Currency         = $Fees_Currency;
		$this->Fees_Amount           = $Fees_Amount;
		$this->DebitedWalletId       = $DebitedWalletId;
		$this->CreditedWalletId      = $CreditedWalletId;
	}

	private function getMangoPayApi() {
		$mangoPayApi  = new MangoPay\MangoPayApi();
		$mangoPayApi->Config->ClientId        = 'frenchconnection2015';
		$mangoPayApi->Config->ClientPassword  = '5CsHKZGBDxv7btyoeMXCbLBKT59VqsJUMSedLTXhv1WveqNBOo';
		$mangoPayApi->Config->TemporaryFolder = __dir__;
		return $mangoPayApi;
	}

	public function createTransfer() {
		$mangoPayApi = $this->getMangoPayApi();

		//Build the parameters for the request
		$Transfer                         = new MangoPay\Transfer();
		$Transfer->AuthorId               = $this->AuthorId;
		$Transfer->DebitedFunds           = new MangoPay\Money();
		$Transfer->DebitedFunds->Currency = $this->DebitedFunds_Currency;
		$Transfer->DebitedFunds->Amount   = $this->DebitedFunds_Amount;
		$Transfer->Fees                   = new MangoPay\Money();
		$Transfer->Fees->Currency         = $this->Fees_Amount;
		$Transfer->Fees->Amount           = $this->Fees_Currency;
		$Transfer->DebitedWalletID        = $this->DebitedWalletId;
		$Transfer->CreditedWalletId       = $this->CreditedWalletId;

		//Send the request
		$result = $mangoPayApi->Transfers->Create($Transfer);

		//Analyse the request
		return $result;
	}

}

//usage
$transfersManager = new ChauffeurCorp_Transfers( 5283321, "EUR", 1503, "EUR", 10, 5284334, 5297522);
$tty              = $transfersManager->createTransfer();
var_dump($tty);
