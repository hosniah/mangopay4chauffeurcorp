<?php
require_once '../MangoPaySDK/mangoPayApi.inc';

class ChauffeurCorp_Payout {

	private $AuthorId;
	private $DebitedWalletID;
	private $DebitedFunds_Currency;
	private $DebitedFunds_Amount;
	private $Fees_Currency;
	private $Fees_Amount;
	private $BankAccountId;

	public function __construct ($AuthorId, $DebitedWalletID, $DebitedFunds_Currency, $DebitedFunds_Amount, $Fees_Currency, $Fees_Amount, $BankAccountId) {
		$this->$AuthorId              = $AuthorId;
		$this->$DebitedWalletID       = $DebitedWalletID;
		$this->$DebitedFunds_Currency = $DebitedFunds_Currency;
		$this->$DebitedFunds_Amount   = $DebitedFunds_Amount;
		$this->$Fees_Currency         = $Fees_Currency;
		$this->$Fees_Amount           = $Fees_Amount;
		$this->$BankAccountId         = $BankAccountId;
	}

	private function getMangoPayApi() {
		$mangoPayApi  = new MangoPay\MangoPayApi();
		$mangoPayApi->Config->ClientId        = 'frenchconnection2015';
		$mangoPayApi->Config->ClientPassword  = '5CsHKZGBDxv7btyoeMXCbLBKT59VqsJUMSedLTXhv1WveqNBOo';
		$mangoPayApi->Config->TemporaryFolder = __dir__;
		return $mangoPayApi;
	}

	public function createPayout() {
		//Create an instance of MangoPayApi SDK
		$mangoPayApi = $this->getMangoPayApi();
		 
		//Build the parameters for the request
		$PayOut = new \MangoPay\PayOut();
		$PayOut->AuthorId = 5283321;
		$PayOut->DebitedWalletID = 5284334;
		$PayOut->DebitedFunds = new \MangoPay\Money();
		$PayOut->DebitedFunds->Currency = "EUR";
		$PayOut->DebitedFunds->Amount = 9145;
		$PayOut->Fees = new \MangoPay\Money();
		$PayOut->Fees->Currency = "EUR";
		$PayOut->Fees->Amount = 6105;
		$PayOut->PaymentType = "BANK_WIRE";
		$PayOut->MeanOfPaymentDetails = new \MangoPay\PayOutPaymentDetailsBankWire();
		$PayOut->MeanOfPaymentDetails->BankAccountId = 5292263;
		 
		//Send the request
		$result = $mangoPayApi->PayOuts->Create($PayOut);
		 
		//Analyse the request
		//var_dump($result);
		return $result;
	}

}

$payoutManager = new ChauffeurCorp_Payout( 5283321, 5284334, "EUR", 9145, "EUR", 6105, 5292263);
$tty           = $payoutManager->createPayout();
var_dump($tty);
