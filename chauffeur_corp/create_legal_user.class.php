<?php
require_once '../MangoPaySDK/mangoPayApi.inc';



class ChauffeurCorp_LegalUser {
private $_Name; 
private $LegalPersonType;
private $HeadquartersAddress;
private $LegalRepresentativeFirstName;
private $LegalRepresentativeLastName;
private $LegalRepresentativeAddress;
private $LegalRepresentativeEmail;
private $LegalRepresentativeBirthday;
private $LegalRepresentativeNationality;
private $LegalRepresentativeCountryOfResidence;
private $Email;
private $Tag;

public function __construct ($Name, $LegalPersonType, $HeadquartersAddress, $LegalRepresentativeFirstName,
			$LegalRepresentativeLastName, $LegalRepresentativeAddress, $LegalRepresentativeEmail,
			$LegalRepresentativeBirthday, $LegalRepresentativeNationality , $LegalRepresentativeCountryOfResidence ,
			$Email= NULL, $Tag= NULL) {

$this->_Name				     = $Name;
$this->LegalPersonType			     = $LegalPersonType;
$this->HeadquartersAddress		     = $HeadquartersAddress;
$this->LegalRepresentativeFirstName          = $LegalRepresentativeFirstName;
$this->LegalRepresentativeLastName	     = $LegalRepresentativeLastName;
$this->LegalRepresentativeAddress            = $LegalRepresentativeAddress;
$this->LegalRepresentativeEmail	             = $LegalRepresentativeEmail;
$this->LegalRepresentativeBirthday	     = $LegalRepresentativeBirthday;
$this->LegalRepresentativeNationality	     = $LegalRepresentativeNationality;
$this->LegalRepresentativeCountryOfResidence = $LegalRepresentativeCountryOfResidence;
$this->Email				     = $Email;
$this->Tag				     = $Tag;
}

	private function getMangoPayApi() {
		$mangoPayApi  = new MangoPay\MangoPayApi();
		$mangoPayApi->Config->ClientId        = 'frenchconnection2015';
		$mangoPayApi->Config->ClientPassword  = '5CsHKZGBDxv7btyoeMXCbLBKT59VqsJUMSedLTXhv1WveqNBOo';
		$mangoPayApi->Config->TemporaryFolder = __dir__;
		return $mangoPayApi;
	}

public function createLegalUser() {
//Create an instance of MangoPayApi SDK
$mangoPayApi = $this->getMangoPayApi();
 
//Build the parameters for the request
$User = new MangoPay\UserLegal();
$User->PersonType = "LEGAL";
$User->LegalPersonType = $this->LegalPersonType;
$User->Name = $this->_Name;
$User->HeadquartersAddress = $this->HeadquartersAddress;
$User->LegalRepresentativeFirstName = $this->LegalRepresentativeFirstName;
$User->LegalRepresentativeLastName = $this->LegalRepresentativeLastName;
$User->LegalRepresentativeAdress = $this->LegalRepresentativeAddress;
$User->LegalRepresentativeEmail = $this->LegalRepresentativeEmail;
$User->LegalRepresentativeNationality = $this->LegalRepresentativeNationality;
$User->LegalRepresentativeCountryOfResidence = $this->LegalRepresentativeCountryOfResidence;
$User->LegalRepresentativeBirthday = $this->LegalRepresentativeBirthday; 


 
$User->Statute = "blabla";
$User->ProofOfRegistration = "blabla";
$User->ShareholderDeclaration = "blabla";

$User->Email = $this->Email;
$User->Tag = $this->Tag;
 
 
//Send the request
$result = $mangoPayApi->Users->Create($User);
 
//Analyse the request
//var_dump($result);
return $result;
}

}

//Usage:
$legalUserManager = new chauffeurCorp_LegalUser( "Lean Software Factory2","ORGANIZATION", "Residence Kaies Boumhel",
 "Ahmed", "HOSNI", "Parc Technologique El Ghazela",
 "contact2@lsf.tn", 1421227177, "TN", "TN");
$tty = $legalUserManager->createLegalUser();
var_dump($tty);
