<?php
require_once '../MangoPaySDK/mangoPayApi.inc';

class ChauffeurCorp_CardPreAuthorizations {
	private $AuthorId;

	public function __construct ($AuthorId, $DebitedFunds_Currency, $DebitedFunds_Amount, $SecureMode, $CardId, $SecureModeReturnURL, $Tag=NULL) {
		$this->AuthorId              = $AuthorId;
		$this->DebitedFunds_Currency = $DebitedFunds_Currency;
		$this->DebitedFunds_Amount   = $DebitedFunds_Amount;
		$this->SecureMode            = $SecureMode;
		$this->CardId                = $CardId;
		$this->SecureModeReturnURL   = $SecureModeReturnURL;
		$this->Tag                   = $Tag;
	}

	private function getMangoPayApi() {
		$mangoPayApi  = new MangoPay\MangoPayApi();
		$mangoPayApi->Config->ClientId        = 'frenchconnection2015';
		$mangoPayApi->Config->ClientPassword  = '5CsHKZGBDxv7btyoeMXCbLBKT59VqsJUMSedLTXhv1WveqNBOo';
		$mangoPayApi->Config->TemporaryFolder = __dir__;
		return $mangoPayApi;
	}

	public function createCardPreAuth() {
		$mangoPayApi = $this->getMangoPayApi();

		//Build the parameters for the request
		$CardPreAuthorization                         = new MangoPay\CardPreAuthorization();
		$CardPreAuthorization->AuthorId               = $this->AuthorId;
		$CardPreAuthorization->DebitedFunds           = new MangoPay\Money();
		$CardPreAuthorization->DebitedFunds->Currency = $this->DebitedFunds_Currency;
		$CardPreAuthorization->DebitedFunds->Amount   = $this->DebitedFunds_Amount;
		$CardPreAuthorization->SecureMode             = $this->SecureMode;
		$CardPreAuthorization->CardId                 = $this->CardId;
		$CardPreAuthorization->SecureModeReturnURL    = $this->SecureModeReturnURL;
		$CardPreAuthorization->Tag                    = $this->Tag;

		//Send the request
		$result = $mangoPayApi->CardPreAuthorizations->Create($CardPreAuthorization);
		//Analyse the request
		return $result;
	}

}

//usage
$preAuthManager = new ChauffeurCorp_CardPreAuthorizations(5283321, "EUR", 294, "DEFAULT", 5300809, "http://www.example.com/file.php", "blabla");
$tty = $preAuthManager->createCardPreAuth();
var_dump($tty);
